from django.test import TestCase, Client
from django.urls import resolve
from django.contrib.auth import get_user_model
from django.contrib.auth.models import User
from .views import accounts, logged_out, register, registered
from .forms import RegisterForm

class TestURL(TestCase):
    
    def setUp(self):
        username = 'tk2ppw'
        email = 'teampepewe@gmail.com'
        password = 'csui2019'
        user = get_user_model().objects.create_user(username=username, email=email, password=password)
        self.client.login(username='tk2ppw', password='csui2019')
        
    # profile
    def test_accounts_url_is_exist(self):
        response = self.client.get('/accounts/')
        self.assertEqual(response.status_code, 200)
    
    def test_using_accounts_template(self):
        response = self.client.get('/accounts/')
        self.assertTemplateUsed(response, 'accounts.html')
    
    def test_using_accounts_view_func(self):
        found = resolve('/accounts/')
        self.assertEqual(found.func, accounts)
        
    # logout
    def test_logged_out_url_is_exist(self):
        response = self.client.get('/accounts/logged_out/')
        self.assertEqual(response.status_code, 200)
        
    def test_using_logged_out_template(self):
        response = self.client.get('/accounts/logged_out/')
        self.assertTemplateUsed(response, 'registration/logged_out.html')
    
    def test_using_logged_out_view_func(self):
        found = resolve('/accounts/logged_out/')
        self.assertEqual(found.func, logged_out)
        
    # register
    def test_register_url_is_exist(self):
        response = self.client.get('/accounts/register/')
        self.assertEqual(response.status_code, 200)
    
    def test_using_register_template(self):
        response = self.client.get('/accounts/register/')
        self.assertTemplateUsed(response, 'registration/register.html')
    
    def test_using_register_view_func(self):
        found = resolve('/accounts/register/')
        self.assertEqual(found.func, register)
    
    def test_registered_url_is_exist(self):
        response = self.client.get('/accounts/registered/')
        self.assertEqual(response.status_code, 200)
    
    def test_using_registered_template(self):
        response = self.client.get('/accounts/registered/')
        self.assertTemplateUsed(response, 'registration/registered.html')
    
    def test_using_registered_view_func(self):
        found = resolve('/accounts/registered/')
        self.assertEqual(found.func, registered)
        
class TestRegister(TestCase):
    
    def setUp(self):
        self.username = 'tk2ppw'
        self.first_name = 'team'
        self.last_name = 'pepewe'
        self.email = 'teampepewe@gmail.com'
        self.password1 = 'iloveppw'
        self.password2 = 'iloveppw'
    
    def test_register_form_is_valid(self):
        form = RegisterForm({
            'username' : self.username,
            'first_name' : self.first_name,
            'last_name' : self.last_name,
            'email' : self.email,
            'password1' : self.password1,
            'password2' : self.password2,
        })
        self.assertTrue(form.is_valid())
        
    def test_register_create_new_user(self):
        response = self.client.post('/accounts/register/', data = {
            'username' : self.username,
            'first_name' : self.first_name,
            'last_name' : self.last_name,
            'email' : self.email,
            'password1' : self.password1,
            'password2' : self.password2,
        })
        users = get_user_model().objects.filter(username='tk2ppw')
        self.assertEqual(users.count(), 1)