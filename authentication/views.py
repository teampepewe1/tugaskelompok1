from django.shortcuts import render
from django.contrib.auth import authenticate
from django.http import HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from .forms import RegisterForm
from reviews.models import Review

@login_required
def accounts(request):
    username = request.user.username
    reviews = Review.objects.filter(name=username)
    ln = reviews.count()
    return render(request, 'accounts.html', {'reviews':reviews, 'ln':ln})

def logged_out(request):
    return render(request, 'registration/logged_out.html')

def register(request):
    if request.method == 'POST':
        form = RegisterForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            first_name = form.cleaned_data.get('first_name')
            last_name = form.cleaned_data.get('last_name')
            email = form.cleaned_data.get('email')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            return HttpResponseRedirect('/registered/')
    else:
        form = RegisterForm()
    return render((request), 'registration/register.html', {'form':form})

def registered(request):
    return render(request,'registration/registered.html')