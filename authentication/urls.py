from django.urls import path, include
from . import views

urlpatterns = [
    path('logged_out/', views.logged_out, name='logged_out'),
    path('', views.accounts, name='accounts'),
    path('', include('django.contrib.auth.urls')),
    path('', include('allauth.urls')),
    path('register/', views.register, name='register'),
    path('registered/', views.registered, name='registered'),
]