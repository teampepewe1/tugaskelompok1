from django.db import models

# Create your models here.
class Coupon(models.Model):
    code = models.CharField(max_length = 30)
    periode = models.DateTimeField()
    percent_discount = models.IntegerField(
        default=0,
     )
    description = models.TextField(max_length=200)

    def __str__(self):
        return self.code