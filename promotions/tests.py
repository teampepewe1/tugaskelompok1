from django.utils import timezone
from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from .views import promotions_view, search_result_view
from .models import Coupon

class TestUrl(TestCase):

    def test_url_is_exist(self):
        response = Client().get('/promotions/')
        self.assertEqual(response.status_code,200)

    def test_using_promotions_template(self):
        response = Client().get('/promotions/')
        self.assertTemplateUsed(response, 'promotions.html')

    def test_using_promotions_view_func(self):
        found = resolve('/promotions/')
        self.assertEqual(found.func, promotions_view)

class TestModel(TestCase):

    def setUp(self):
        #Creating a new coupon
        Coupon.objects.create(code='WKWK',periode=timezone.now(),percent_discount=100,description='INI ADALAH KUPON SAKTI YANG BISA KASIH DISKON 100%')
        Coupon.objects.create(code='WKWKA',periode=timezone.now(),percent_discount=50,description='INI ADALAH KUPON SAKTI YANG BISA KASIH DISKON 50%')

    def test_str_is_equal_to_title(self):
        coupon = Coupon.objects.get(code='WKWKA')
        self.assertEqual(str(coupon), coupon.code)

    def test_model_can_create_new_objects(self):
        #Retrieving all available coupon
        counting_all_available_activity = Coupon.objects.all().count()
        self.assertEqual(counting_all_available_activity,2)

    def test_model_get_twin_code(self):
        Coupon.objects.create(code='WKWK',periode=timezone.now(),percent_discount=100,description='INI ADALAH KUPON SAKTI YANG BISA KASIH DISKON 100%')
        code = Coupon.objects.filter(code = 'WKWK')
        self.assertEqual(code.count(),2)
    
    def test_views_promotions_form(self):
        response = Client().get('/promotions/')
        self.assertContains(response, '<form')
        self.assertContains(response, '<select')
        self.assertContains(response, '<input')
        self.assertContains(response, '<table')
        self.assertContains(response, '<thead')
        self.assertContains(response, '<tbody')


class TestSearchView(TestCase):

    def setUp(self):
        self.response = Client().get('/promotions/search/')
        Coupon.objects.create(code='WKWK',periode=timezone.now(),percent_discount=100,description='INI ADALAH KUPON SAKTI YANG BISA KASIH DISKON 100%')
        Coupon.objects.create(code='WKWK',periode=timezone.now(),percent_discount=50,description='INI ADALAH KUPON SAKTI YANG BISA KASIH DISKON 50%')
    
    def test_views_promotions_search_form(self):
        self.assertContains(self.response, '<form')
        self.assertContains(self.response, '<select')
        self.assertContains(self.response, '<input')
        self.assertContains(self.response, '<table')
        self.assertContains(self.response, '<thead')
    
    def test_search_input_client(self):
        response = Client().get('/promotions/search/?dropdown=kode&input=WKWK')
        self.assertContains(response, 'WKWK', status_code=200)




