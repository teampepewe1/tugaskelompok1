from django.urls import path
from . import views


urlpatterns = [
    path('', views.promotions_view, name='promotions_view'),
    path('search/', views.search_result_view, name='search_result')
    
]