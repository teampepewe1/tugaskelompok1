from django.shortcuts import render
from django.db.models import Q

from .models import Coupon

# Create your views here.
def promotions_view (request):
    coupons = Coupon.objects.all()
    
    context = {
        'coupons' : coupons,
    }
    return render (request, 'promotions.html', context)

def search_result_view (request):
    query = request.GET.get('input')
    dropdown = request.GET.get('dropdown')
    not_int_input = False
    if dropdown == 'kode':
        coupons = Coupon.objects.filter(code__icontains=query)
    else:
        try:
            coupons = Coupon.objects.filter(percent_discount__gte = query)
        except:
            not_int_input = True
            return render(request,'promotions.html',{'not_int_input':not_int_input})
            

    context = {
        'coupons' : coupons,
        'not_int_input': not_int_input,
    }
    return render (request, 'promotions.html', context)



