from django.db import models
from products.models import Products

RATING = [
	("1", "1 - Terrible"),
	("2", "2 - Poor"),
	("3", "3 - Ok"),
	("4", "4 - Good"),
	("5", "5 - Excellent")
]

class Review(models.Model):
	name = models.CharField(max_length = 64)
	product = models.ForeignKey(Products, on_delete=models.CASCADE)
	rating = models.CharField(max_length = 64, choices=RATING)
	review = models.TextField()
	date = models.DateTimeField(auto_now_add=True)