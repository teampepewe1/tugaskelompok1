from django import forms
from .models import Review
from products.models import Products 

class ReviewForm(forms.ModelForm):
	class Meta:
		model = Review
		exclude = ["name", "date"]
		widgets = {
			'review': forms.Textarea(attrs={'class':'responsive-text-area'})
				}