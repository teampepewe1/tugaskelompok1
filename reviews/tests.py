from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import review
from .models import Review

# Create your tests here.
class ReviewTest(TestCase):

	def test_review_url_is_exist(self):
		response = Client().get('/reviews/')
		self.assertEqual(response.status_code,200)
	
	def test_review_using_index_func(self):
		found = resolve('/reviews/')
		self.assertEqual(found.func, review)
		
	def test_landing_page_content_is_written(self):
		self.assertIsNotNone(review)
		
	def test_review_using_template(self):
		response = Client().get('/reviews/')
		self.assertTemplateUsed(response, 'reviews.html')
		
#	def test_thank_you_url_is_exist(self):
#        response = Client().get('/reviews/thankyou/')
#        self.assertEqual(response.status_code,200)