from django.shortcuts import render
from .models import Review
from .forms import ReviewForm


def review(request):
    
    if(request.method == 'POST'):
        form = ReviewForm(request.POST or None)
        response = {}
        if form.is_valid():
            f = form.save(commit=False)
            f.name = request.user.username
            f.save()
            response['form'] = f
            return render(request, 'thankyou.html', response)
    else:
        form = ReviewForm()
        return render(request, 'reviews.html', {'form':form})