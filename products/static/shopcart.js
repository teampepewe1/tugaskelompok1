let carts = document.querySelectorAll('.btn-primary');

let item = document.querySelectorAll('#nama');

let products =[
    {
        nama: "tas pink",
        harga : 100,
        incart :0,
        gambar : "https://cdn.idntimes.com/content-images/post/20190811/humble-ghent-8fa75f66905b0c1caca5f63e25a83ecd.jpg"
    },
    {
        nama: "tas plastik",
        harga : 200,
        incart :0,
        gambar : "https://id-test-11.slatic.net/p/aade0acdcdff08196ad5029d54ac19c2.jpg"
    },
    {
        nama: "tas serut",
        harga : 300,
        incart :0,
        gambar : "https://asset-a.grid.id/crop/0x0:0x0/700x0/photo/2018/05/08/1534757273.jpg"
    },
    {
        nama: "tas ransel",
        harga : 400,
        incart :0,
        gambar : "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTFUDxv24dhwcLa7K81w7lavby4PdAvmFUtr-4gEE4DIeA89zLb&usqp=CAU"
    }
]

for(let i=0;i < carts.length;i++) {
    carts[i].addEventListener('click',()=>{
        cartnumbers(products[i]);
    });
}

function cartnumbers(product) {
    let productnumbers = localStorage.getItem('cartnumbers');
    productnumbers = parseInt(productnumbers);
    if(productnumbers) {
        localStorage.setItem('cartnumbers',productnumbers + 1)
        alert("Item added to your cart!");
    } else {
        localStorage.setItem('cartnumbers',1);
        alert("Item added to your cart !")
    }
    setItems(product);
}

function setItems(product) {
    let cartItems = localStorage.getItem('productsincart')
    cartItems = JSON.parse(cartItems);

    if(cartItems !== null){
        if( cartItems[product.nama] == undefined) {
            cartItems = {
                ...cartItems,
                [product.nama]: product
            }
        }
        cartItems[product.nama].incart += 1; 
    } else {
        product.incart = 1;
        cartItems = {
            [product.nama] : product
        }
    } 
    localStorage.setItem("productsincart",JSON.stringify(cartItems));
}

function displaycart() {
    let cartItems = localStorage.getItem("productsincart");
    cartItems = JSON.parse(cartItems);
    let productContainer = document.querySelector(".products"); 
    if(cartItems && productContainer) {
        productContainer.innerHTML = '';
        productContainer.innerHTML += `<button onclick="hapus()" class="btn btn-danger">x</button><br>`
        Object.values(cartItems).map(item => {
            productContainer.innerHTML +=`
            <tr>
            <td><span class="unit">${item.nama}</span></td>
            <td><button class="btn btn-info">-</button></td>
            <td><span>${item.incart}</span></td>
           </tr><br><br><br>`
        });
    }
}
displaycart();

// let increase = document.querySelectorAll('.btn-light');
let decrease = document.querySelectorAll('.btn-info');

for(let i=0;i < decrease.length;i++) {
    decrease[i].addEventListener('click',()=>{
        kurangitem(products[i]);
    });
}

function kurangitem(product) {
    let productnumbers = localStorage.getItem('cartnumbers');
    productnumbers = parseInt(productnumbers);
    localStorage.setItem('cartnumbers',productnumbers - 1)
    apusitems(product);
}

function apusitems(product) {
    let cartItems = localStorage.getItem('productsincart')
    cartItems = JSON.parse(cartItems);
    if(cartItems[product.nama].incart > 1){
        cartItems[product.nama].incart -= 1; 
    } else {
        alert("you can't decrease 1 item, remove")
    }
    localStorage.setItem("productsincart",JSON.stringify(cartItems));
    displaycart()
}

// for(let i=0;i < increase.length;i++) {
//     increase[i].addEventListener('click',()=>{
//         tambah(products[i]);
//     });
// }

// for(let i=0;i < decrease.length;i++) {
//     decrease[i].addEventListener('click',()=>{
//         kurang(products[i]);
//     });
// }


// function kurang(product) {
//     let cartItems = localStorage.getItem('productsincart')
//     cartItems = JSON.parse(cartItems);
//     if([product.nama].incart > 1){
//         cartItems[product.nama].incart -= 1; 
//     } else {
//         alert('buyer at least purchase one item or remove it from the cart');
//     }
//     displaycart();
// }

// function tambah(product) {
//     console.log(product)
//     let cartItems = localStorage.getItem('productsincart')
//     cartItems = JSON.parse(cartItems);
//     console.log(product)
//     cartItems[product.nama].incart += 1; 
//     displaycart();
// }


let remove = document.querySelectorAll('.btn-danger');

for(let i=0;i < remove.length;i++) {
    remove[i].addEventListener('click',()=>{
        hapus(products[i]);
    });
}

function hapus() {
    localStorage.removeItem('productsincart');
    localStorage.removeItem('cartnumbers');
    displaycart();
}
