# Generated by Django 3.0.3 on 2020-05-13 14:06

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0006_auto_20200305_2037'),
    ]

    operations = [
        migrations.AlterField(
            model_name='products',
            name='harga',
            field=models.DecimalField(decimal_places=2, max_digits=12),
        ),
    ]
