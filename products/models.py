from django.db import models

class Products(models.Model) :
    gambar = models.URLField(max_length=300, null=True)
    namaBarang = models.CharField(max_length = 50)
    harga = models.DecimalField(decimal_places=2, max_digits=12)
    stok = models.PositiveIntegerField()
    deskripsi = models.CharField(max_length = 200)

    def __str__(self):
        return self.namaBarang

# class Keranjang(models.Model) :
#     gambar = models.URLField(max_length=300, null=True)
#     title = models.CharField(max_length = 50)
#     desc = models.CharField(max_length = 200)