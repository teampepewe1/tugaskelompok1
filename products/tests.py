from django.test import TestCase, Client
from django.urls import resolve
from .views import products, search_result
from .models import Products
from .apps import ProductsConfig

# Create your tests here.
class TestProducts(TestCase):
	def test_products_url_is_exist(self):
		response = Client().get('/products/')
		self.assertEqual(response.status_code, 200)
	def test_search_products_url_is_exist(self):
		Products.objects.create(
			gambar="https://bajuyuli.com/uploads/products/data/14-bajuyuli-baju-muslim-anak-perempuan-jersey-hitam.jpg",
			namaBarang="baju",
			harga="1.000",
			stok="3",
			deskripsi="Baju canggih"
		)
		response = Client().get('/products/search/?hasil_cari=baju')
		self.assertEqual(response.status_code, 200)
	def test_products_using_template(self):
		response = Client().get('/products/')
		self.assertTemplateUsed(response, 'test.html')

class TestFunc(TestCase):
	def test_views_func(self):
		found = resolve('/products/')
		self.assertEqual(found.func, products)

class TestModels(TestCase):
	def test_models(self):
		new = Products.objects.create(
			gambar="https://bajuyuli.com/uploads/products/data/14-bajuyuli-baju-muslim-anak-perempuan-jersey-hitam.jpg",
			namaBarang="baju",
			harga="1.000",
			stok="3",
			deskripsi="Baju canggih"
		)
		counting_all_available_todo = Products.objects.all().count()
		self.assertEqual(counting_all_available_todo, 1)

class TestApp(TestCase):
	def test_app(self):
		self.assertEqual(ProductsConfig.name, "products")

class TestStr(TestCase):
	def test_str_is_equal_to_title(self):
		new = Products.objects.create(
			gambar="https://bajuyuli.com/uploads/products/data/14-bajuyuli-baju-muslim-anak-perempuan-jersey-hitam.jpg",
			namaBarang="baju",
			harga="1.000",
			stok="3",
			deskripsi="Baju canggih"
			)
		barang = Products.objects.get(namaBarang='baju')
		self.assertEqual(str(barang), barang.namaBarang)



#unittest
from django.test import TestCase, Client, override_settings
from django.urls import reverse, resolve
from .views import *

# functional testing.
from django.test import TestCase
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time

#Create your tests here.
@override_settings(STATICFILES_STORAGE='django.contrib.staticfiles.storage.StaticFilesStorage')
class TestModelsActivity(TestCase):
    def test_post(self):
        self.client = Client()
        response = self.client.post('')
        self.assertEqual(response.status_code, 200)

    def test_use_func(self):
        found = resolve('')
        self.assertEquals(found.func,products)
