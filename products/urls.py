from django.urls import path
from . import views

 
app_name = 'products'

urlpatterns = [
    path('search/', views.search_result, name='search_result'),
    path('', views.products, name='products'),
    path('shop.html/', views.cartpage, name = 'shop'),
]