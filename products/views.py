from django.shortcuts import render
from .models import Products
# Create your views here.
def products(request):
    cari = Products.objects.all()
    context = {
        'cari':cari
    }
    return render(request, 'test.html', context)

def search_result(request):
    produk = request.GET.get("hasil_cari")
    cari = Products.objects.filter(namaBarang__icontains=produk)
    context = {
        'cari':cari
    }
    return render(request, 'test.html', context)

def cartpage(request):
    return render(request,'shop.html')
