from django.db import models

# from products.models import Products 
# from promotions.models import Coupon

class Cart(models.Model):
    nama = models.CharField(max_length = 200, default="") 
    harga = models.PositiveIntegerField(default=0)
    jumlah = models.PositiveIntegerField(default=1)
    gambar =  models.TextField(default = "", max_length= 500)
