$(function(){
    $("#search").keyup(function(){
        // alert("dww")

        $.ajax({
            type    : "POST",
            url     : "/productsdetails/coupon/search/",
            data    : {
                'search_text'           : $('#search').val(),
                'search_option'         : $('#search_option').val(),
                'csrfmiddlewaretoken'   : $("input[name=csrfmiddlewaretoken]").val()
            },
            success : searchSuccess,
            dataType : 'html'
        });
    });

    $(document).on('click','.hasil-cari',function(){
        $("#search").val($(this).text());
        $("#search_results").html('')
    });

    $('#apply-btn').click(function(){
        $.ajax({
            type    : "POST",
            url     : "/productsdetails/coupon/apply/",
            data    : {
                'coupon'           : $('#search').val(),
                'harga-barang'     : $('#harga-barang').html(),
                'csrfmiddlewaretoken'   : $("input[name=csrfmiddlewaretoken]").val()
            },
            success : function(data){
                $("#search_results").html('')
                $('#harga-barang').replaceWith(data)
            },
            dataType: 'html'
        })
    });
});

function searchSuccess(data, textStatus, jqXHR){
    $('#search_results').html(data);
}
