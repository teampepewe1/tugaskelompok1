# Generated by Django 3.0.3 on 2020-03-03 04:40

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('productsdetails', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='productsdetails',
            name='gambar',
            field=models.ImageField(default='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAARMAAAC3CAMAAAAGjUrGAAAAA1BMVEX///+nxBvIAAAAR0lEQVR4nO3BAQ0AAADCoPdPbQ8HFAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAPBgxUwAAU+n3sIAAAAASUVORK5CYII=', upload_to=''),
        ),
        migrations.AlterField(
            model_name='productsdetails',
            name='harga',
            field=models.CharField(max_length=20),
        ),
    ]
