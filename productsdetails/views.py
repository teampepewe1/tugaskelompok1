from django.shortcuts import render, redirect
from products.models import Products
from django.http import HttpResponse
from .models import Cart
# from .forms import CartForm
# from promotions.models import Coupon

# Create your views here.
def productsdetails(request,pk):
    obj = Products.objects.get(id=pk)
    context = {
        'row' : obj,
    }
    return render(request, "productsver.html", context)

def itemtocart(request):
    if request.method == 'POST':
        nama = request.POST['namabarang']
        harga = request.POST['hargabarang']
        jumlah = request.POST['jumlahbarang']
        gambar = request.POST['gambarbarang']

    try:
        data = Cart.objects.get(nama=nama)
        data.jumlah+=1
        data.save()
        
    except:
        Cart.objects.create(
            nama = nama,
            harga = harga,
            jumlah = jumlah,
            gambar = gambar,
        )
    return HttpResponse('')

def MyCart(request) :
    
    buy = Cart.objects.all()
    context = {
        'item': buy,
    }

    # # harga = obj.harga * (100-)
    return render(request, "cart.html", context)

def getProduct(request, id=1):
    return render(request, "cart.html", {'coupon':Coupon.objects.get(id=id)})

def coupon(request):
    if request.method == "POST":
        search_text = request.POST.get("search_text",False)
        search_option = request.POST.get("search_option",False)
    else:
        search_text = ''


    input_not_int = False
    if(search_option == "kode"):
        coupons = Coupon.objects.filter(code__icontains = search_text)
    else:
        try:
            coupons = Coupon.objects.filter(percent_discount__gte = search_text)
            print(coupons)
        except:
            input_not_int = True
            return render(request, "ajax_search.html",{'input_not_int':input_not_int})
    return render(request, "ajax_search.html",{'coupons':coupons})

def apply(request):
    if request.method == "POST":
        code = request.POST.get("coupon",False)
        harga = request.POST.get("harga-barang", False)
        try:
            coupon = Coupon.objects.get(code = code)
            print("coupon ", coupon)
            harga_setelah_diskon = float(harga) * ((100-coupon.percent_discount)/100)
            print(harga_setelah_diskon)
        except:
            harga_setelah_diskon = harga

        print("harga ",harga)

    return render(request, "hasil_diskon.html", {"harga_setelah_diskon": harga_setelah_diskon})
    return render(request, "keranjang.html", context)
