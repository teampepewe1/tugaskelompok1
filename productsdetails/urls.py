from django.urls import include, path
from . import views

app_name = 'productsdetails'

urlpatterns = [
    path('<int:pk>/', views.productsdetails, name='details'),
    path('product/get/<int:id>', views.getProduct, name='get_product'),
    path('coupon/search/', views.coupon, name='coupon'),
    path('coupon/apply/', views.apply, name='apply'),
    path('MyCart/', views.MyCart, name='MyCart'),
    path('addItem/', views.itemtocart, name='additem')
]

