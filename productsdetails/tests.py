from django.test import TestCase,Client
from django.urls import reverse, resolve
from products.models import Products 
from . import urls  
from productsdetails.views import productsdetails

# Create your tests here.
class TestModels(TestCase):
    def test_model(self,namaBarang="Kemeja", harga= 50000.00,stok= "20",deskripsi="baju baru"):
        return Products.objects.create(namaBarang = namaBarang, harga = harga, stok = stok, deskripsi = deskripsi)

    def test_create(self):
        create = self.test_model()
        self.assertTrue(isinstance(create, Products))

class TestURL(TestCase):
    def test_tk_url_is_exist(self):
        product = Products.objects.create(gambar="https://cf.shopee.co.id/file/a41584d765d1320f2eee05a0fce797f6",namaBarang="Kemeja", harga= 150000.00,stok= 20,deskripsi="baju baru")
        response = Client().get('/productsdetails/1/')
        self.assertEqual(response.status_code,200)

    def test_tk_use_temp(self):
        product = Products.objects.create(gambar="https://cf.shopee.co.id/file/a41584d765d1320f2eee05a0fce797f6",namaBarang="Kemeja", harga= 150000.00,stok= 20,deskripsi="baju baru")
        response = Client().get('/productsdetails/1/')
        self.assertTemplateUsed(response,'productsver.html')


from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from django.test import LiveServerTestCase

from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time
from django.utils import timezone

from promotions.models import Coupon

class CouponFunctionalTest(LiveServerTestCase):
    
    def setUp(self):
        chrome_options= Options()
        # chrome_options.add_argument('--dns-prefetch-disable')
        # chrome_options.add_argument('--no-sandbox')        
        # chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(CouponFunctionalTest, self).setUp()
    
    def tearDown(self):
        self.selenium.quit()
        super(CouponFunctionalTest, self).tearDown()

    def test_applied_coupon(self):

        selenium = self.selenium
        openUrl = selenium.get(self.live_server_url + '/')

        # init products
        Products.objects.create(gambar="https://bajuyuli.com/uploads/products/data/14-bajuyuli-baju-muslim-anak-perempuan-jersey-hitam.jpg",namaBarang="baju",harga="1000",stok="3",deskripsi="Baju canggih")\
        # init coupon
        Coupon.objects.create(code='WKWKA',periode=timezone.now(),percent_discount=50,description='INI ADALAH KUPON SAKTI YANG BISA KASIH DISKON 50%')
        Coupon.objects.create(code='WKWOO' ,periode=timezone.now(),percent_discount=30,description='INI ADALAH KUPON SAKTI YANG BISA KASIH DISKON 30%')

        # find element
        selenium.find_element_by_link_text("PRODUCTS").click()
        selenium.find_element_by_link_text("baju").click()
        selenium.find_element_by_link_text("Add to Cart").click()

        selenium.find_element_by_id("search").send_keys('WK')
        time.sleep(2)
        selenium.find_element_by_xpath(u"//p[contains(text(), 'WKWKA')]").click()

        selenium.find_element_by_id("apply-btn").click()
        time.sleep(1)

        # Check if the input is created
        gotPage = selenium.page_source
        assert '500' in gotPage        

        self.assertTemplateUsed(response,'productsver.html')
