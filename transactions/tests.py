from django.utils import timezone
from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from .views import transactions_view
'''
from .models import Transactions

# Create your tests here.
class TestUrl(TestCase):

    def test_url_is_exist(self):
        response = Client().get('/transactions/')
        self.assertEqual(response.status_code,200)

    def test_using_transactions_template(self):
        response = Client().get('/transactions/')
        self.assertTemplateUsed(response, 'transactions.html')

    def test_using_transactions_view_func(self):
        found = resolve('/transactions/')
        self.assertEqual(found.func, transactions_view)

class TestModel(TestCase):
    def setUp(self):
        #Creating a new coupon
        Transactions.objects.create(buyer='Hugo', product='baju', purchase_date=timezone.now(), coupon='WKAIEWASK', total=230000)
        Transactions.objects.create(buyer='HugoIr', product='baju', purchase_date=timezone.now(), coupon='WKAIEWASK', total=230000)

    def test_str_is_equal_to_title(self):
        transaction = Transactions.objects.get(buyer='Hugo')
        self.assertEqual(str(transaction), transaction.buyer)

    def test_model_can_create_new_objects(self):
        #Retrieving all available coupon
        counting_all_available_activity = Transactions.objects.all().count()
        self.assertEqual(counting_all_available_activity,2)

    def test_model_get_twin_code(self):
        Transactions.objects.create(buyer='HugoIr', product='baju', purchase_date=timezone.now(), coupon='WKAIEWASK', total=230000)
        transaction = Transactions.objects.filter(buyer = 'HugoIr')
        self.assertEqual(transaction.count(),2)
    
    def test_views_transactions_form(self):
        response = Client().get('/transactions/')
        self.assertContains(response, '<table')
        self.assertContains(response, '<h1')
        self.assertContains(response, '<thead')
        self.assertContains(response, '<tbody')

'''
    

    