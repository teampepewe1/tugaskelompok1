from django.shortcuts import render
from .models import Transactions

# Create your views here.

def transactions_view(request):
    transactions = Transactions.objects.all()

    context = {
        'transactions' : transactions,
    }
    return render(request,'transactions.html',context)
