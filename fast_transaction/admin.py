from django.contrib import admin
from .models import TransaksiOrang, Fast_Transactions

# Register your models here.
admin.site.register(Fast_Transactions)
admin.site.register(TransaksiOrang)
