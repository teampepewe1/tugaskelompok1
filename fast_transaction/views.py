from django.shortcuts import render
from django.contrib.auth.models import User
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from keranjang.models import Keranjang
from .models import Fast_Transactions
from promotions.models import Coupon
from django.contrib.auth.decorators import login_required
from promotions.models import Coupon

@login_required(login_url='/login/')
def bertransaksi(request):
    semua_barang = request.user.keranjang.dalam_keranjang
    isi_keranjang = []
    total_pembayaran = 0
    for barang in semua_barang.all():
        isi_keranjang.append(barang)
        total_pembayaran += barang.product.harga * barang.total

    context = {
        "total_harga" : total_pembayaran,
        "isi_keranjang" : isi_keranjang
    }
    return render(request, "transaksiCepat.html", context)


def get_total_pembayaran(transaksi):
    total_pembayaran = 0
    for barang in transaksi.product.all():
        total_pembayaran += transaksi.product.harga * barang.total
    try:
        return int(total_pembayaran - (total_pembayaran*(100-transaksi.code_kupon.percent_discount)/100))
    except:
        return total_harga


@login_required(login_url='/login/')
def history_user(request):
    transaksi_cepat = Fast_Transactions.objects.filter(user = request.user).order_by("-transaction_date").all()
    list_transaksi = []
    for transaksi in transaksi_cepat:
        list_transaksi.append(list(transaksi, get_total_pembayaran(transaksi),  ", ".join(map(str, transaksi.product.all()))))
    return render(request, "historyUser.html", {"list_transaksi" : list_transaksi})

@csrf_exempt
def cek_transaksi(request):
    if request.method == "POST" and request.is_ajax():
        semua_barang = request.user.keranjang.dalam_keranjang
        kekurangan_stok = []
        for barang in semua_barang.all():
            total_pembelian = barang.total
            stok = barang.product.stok
            if stok < total_pembelian:
                kekurangan_stok.append(barang.product.namaBarang)
        if len(kekurangan_stok) > 0:
            return JsonResponse({"notifikasi" : "gagal", "gagal_transaksi" : kekurangan_stok})

        #jika tidak gagal
        try:
            dipilih = Coupon.objects.get(code = request.POST["code_kupon"])
        except:
            dipilih = None
        transaksi = Fast_Transactions.objects.create(user = request.user, code_kupon = dipilih)
        for barang in semua_barang.all():
            transaksi.product.add(barang)
            barang.product.stok -= barang.total
            barang.product.save()
        transaksi.save()
        semua_barang.clear() #karena berhasil transaksi
        return JsonResponse({"notifikasi" : "berhasil"})


def coupon(request):
    if request.method == "POST":
        search_text = request.POST.get("search_text",False)
        search_option = request.POST.get("search_option",False)
    else:
        search_text = ''


    input_not_int = False
    if(search_option == "kode"):
        coupons = Coupon.objects.filter(code__icontains = search_text)
    else:
        try:
            coupons = Coupon.objects.filter(percent_discount__gte = search_text)
            print(coupons)
        except:
            input_not_int = True
            return render(request, "ajax_search.html",{'input_not_int':input_not_int})
    return render(request, "ajax_search.html",{'coupons':coupons})

def apply(request):
    if request.method == "POST":
        code = request.POST.get("coupon",False)
        harga = request.POST.get("harga-barang", False)
        try:
            coupon = Coupon.objects.get(code = code)
            print("coupon ", coupon)
            harga_setelah_diskon = float(harga) * ((100-coupon.percent_discount)/100)
            print(harga_setelah_diskon)
        except:
            harga_setelah_diskon = harga

        print("harga ",harga)

    return render(request, "hasil_diskon.html", {"harga_setelah_diskon": harga_setelah_diskon})