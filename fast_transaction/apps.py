from django.apps import AppConfig


class FastTransactionConfig(AppConfig):
    name = 'fast_transaction'
