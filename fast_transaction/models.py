from django.db import models
from django.contrib.auth.models import User
from keranjang.models import Keranjang, Transaction
from promotions.models import Coupon
from products.models import Products

# Create your models here.
class Fast_Transactions(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    product = models.ManyToManyField(Transaction)
    code_kupon = models.ForeignKey(Coupon, on_delete=models.CASCADE, blank=True, null=True, default= None)
    transaction_date = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.user

class TransaksiOrang(models.Model):
    @property
    def harga(self):
        try:
            return self.product.harga * (100 - self.code_kupon.percent_discount) // 100
        except:
            return self.product.harga

    buyer = models.TextField(max_length=50)
    product = models.ForeignKey(Products, on_delete=models.PROTECT)
    total_transaksi = harga
    code_kupon = models.ForeignKey(Coupon,on_delete=models.PROTECT,blank=True, null=True)
    transaction_date = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.buyer

