from django.test import TestCase

# Create your tests here.
from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from .views import bertransaksi
from .models import Fast_Transactions, TransaksiOrang
from keranjang.models import Keranjang, Transaction
from promotions.models import Coupon
from .apps import FastTransactionConfig
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import time

class UnitTest(TestCase):
    def test_app(self):
        self.assertEqual(FastTransactionConfig.name, "fast_transaction")
    def test_bertransaksi_url_is_exist(self):
        response = Client().get('fast_transaction/bertransaksi/')
        self.assertEqual(response.status_code, 200)
    def test_bertransaksi_using_template(self):
        response = Client().get('fast_transaction/bertransaksi/')
        self.assertTemplateUsed(response, 'transaksiCepat.html')
    def test_bertransaksi_func(self):
        found = resolve('fast_transaction/bertransaksi/')
        self.assertEqual(found.func, bertransaksi)

    def test_fast_transaction_models(self):
        usernya = User.objects.create_user("myUser", password = "inipassword")
        myProduct = Products.objects.create(
            gambar="https://bajuyuli.com/uploads/products/data/14-bajuyuli-baju-muslim-anak-perempuan-jersey-hitam.jpg",
			namaBarang="baju",
			harga="10.000",
			stok="3",
			deskripsi="Baju canggih"
		)

        myTransaksi = Transaction.objects.create(product = myProduct, total = 3)
        myKupon = Coupon.objects.create(
            code='WKWK',
            periode=timezone.now(), 
            percent_discount=100, 
            description='INI ADALAH KUPON SAKTI YANG BISA KASIH DISKON 100%')

        myFastTransaction = Fast_Transactions.objects.create(
            user=usernya, 
            product=myTransaksi, 
            code_kupon=myKupon,
            transaction_date=timezone.now())
        self.assertEqual(Fast_Transactions.objects.all().count(), 1)
        self.assertEqual(message.__str__(), "myUser")

    def test_transaksiOrang_models(self):
        usernya = User.objects.create_user("myUser", password = "inipassword")
        myProduct = Products.objects.create(
            gambar="https://bajuyuli.com/uploads/products/data/14-bajuyuli-baju-muslim-anak-perempuan-jersey-hitam.jpg",
			namaBarang="baju",
			harga="10.000",
			stok="3",
			deskripsi="Baju canggih"
		)
        myKupon = Coupon.objects.create(
            code='WKWK',
            periode=timezone.now(), 
            percent_discount=100, 
            description='INI ADALAH KUPON SAKTI YANG BISA KASIH DISKON 100%')

        transaksiOrang = TransaksiOrang.objects.create(
            buyer="Naruto", 
            product=myProduct, 
            code_kupon=myKupon, 
            transaction_date=timezone.now())
        self.assertEqual(TransaksiOrang.objects.all().count(), 1)
        self.assertEqual(message.__str__(), "Naruto")
            

class FunctionalTest(LiveServerTestCase):
    def setUp(self):
        super().setUp()
        chrome_options = Options()
        chrome_options.add_argument("--window-size=1920,1080")
        chrome_options.add_argument('--dns-prefectch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        service_log_path='./chromedriver.log'
        service_args=['--verbose']
        self.browser = webdriver.Chrome('./chromedriver', chrome_options = chrome_options)
        time.sleep(1)

    def tearDown(self):
        self.browser.quit()
        super().tearDown()




from django.test import TestCase, Client
from django.urls import resolve
from products.models import Products
# Smart Coupon Test

class CouponUnitTest(TestCase):

    # test that smart coupon cannot be accessed from direct url
    def test_cant_direct_access_url_search(self):
        response = Client().get('/coupon/search/')
        self.assertEqual(response.status_code, 404)
    
    def test_cant_direct_access_url_apply(self):
        response = Client().get('/coupon/apply/')
        self.assertEqual(response.status_code, 404)

  

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from django.test import LiveServerTestCase

from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time
from django.utils import timezone

from promotions.models import Coupon

class CouponFunctionalTest(LiveServerTestCase):
    
    def setUp(self):
        chrome_options= Options()
        # chrome_options.add_argument('--dns-prefetch-disable')
        # chrome_options.add_argument('--no-sandbox')        
        # chrome_options.add_argument('--headless')
        # chrome_options.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(CouponFunctionalTest, self).setUp()
    
    def tearDown(self):
        self.selenium.quit()
        super(CouponFunctionalTest, self).tearDown()

    def test_applied_coupon(self):

        selenium = self.selenium
        openUrl = selenium.get(self.live_server_url + '/')

        # init products
        Products.objects.create(gambar="https://bajuyuli.com/uploads/products/data/14-bajuyuli-baju-muslim-anak-perempuan-jersey-hitam.jpg",namaBarang="baju",harga="1000",stok="3",deskripsi="Baju canggih")\
        # init coupon
        Coupon.objects.create(code='WKWKA',periode=timezone.now(),percent_discount=50,description='INI ADALAH KUPON SAKTI YANG BISA KASIH DISKON 50%')
        Coupon.objects.create(code='WKWOO' ,periode=timezone.now(),percent_discount=30,description='INI ADALAH KUPON SAKTI YANG BISA KASIH DISKON 30%')

        # find element
        selenium.find_element_by_link_text("PRODUCTS").click()
        selenium.find_element_by_link_text("baju").click()
        selenium.find_element_by_link_text("Add to Cart").click()

        selenium.find_element_by_id("search").send_keys('WK')
        time.sleep(2)
        selenium.find_element_by_xpath(u"//p[contains(text(), 'WKWKA')]").click()

        selenium.find_element_by_id("apply-btn").click()
        time.sleep(1)

        # Check if the input is created
        gotPage = selenium.page_source
        assert '500' in gotPage        

