from django.urls import path
from . import views

app_name = 'fast_transaction'

urlpatterns = [
    path('bertransaksi/', views.bertransaksi, name="bertransaksi"),
    path("cek_transaksi/", views.cek_transaksi, name="cek_transaksi"),
    path("historyKamu/", views.history_user, name="history_user"),
    path('coupon/search/', views.coupon, name='coupon'),
    path('coupon/apply/', views.apply, name='apply'),
]
