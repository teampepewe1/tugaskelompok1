$(document).ready(function(e) {
    $("#loading").css({"display":"block"})

    $("#checkout-btn").click(function(){
        $.ajax({
            url: "/fast_transaction/transaksi/",
            type: "POST",
            data: {"code_kupon" :  "None"}, //untuk sementara
            // data: {"code_kupon" : $(".search_kupon").text() ? $(".search_kupon").text() : "None"}, //kalau sudah ada search kupon
            success: function(result) {
                $(".alert").css({"display":"none"})
                if (result["notifikasi"] == "berhasil"){
                    $(".notifikasi").append("Transaksi berhasil, terima kasih :D")
                    $("#checkout").attr("disabled", true)

                }else if (result["notifikasi"] == "gagal") {
                    $(".notifikasi").append("Transaksi gagal :(")
                    $(".gagal_barang").text("Barang yang gagal adalah " + result["gagal_transaksi"].join(", "))
                }
            }
        })
       
    })
})
