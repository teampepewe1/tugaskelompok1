from django.db import models
from django.contrib.auth.models import User
from products.models import Products

# Create your models here.
class Transaction(models.Model):
    product = models.ForeignKey(Products, on_delete=models.CASCADE)
    total = models.PositiveIntegerField()

    def __str__(self):
        return self.product.namaBarang

class Keranjang(models.Model):
    kepunyaan_keranjang = models.OneToOneField(User, on_delete=models.CASCADE)
    dalam_keranjang = models.ManyToManyField(Transaction)

    def __str__(self):
        return self.nama_keranjang.username
